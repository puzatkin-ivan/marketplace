<?php

namespace App\EventDispatcher;

use App\Event\ChangedProductEvent;
use App\Service\MailerService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ProductSubscriber implements EventSubscriberInterface
{
    /**
     * @var MailerService
     */
    private $mailer;

    public static function getSubscribedEvents(): array
    {
        return [
            ChangedProductEvent::NAME => 'onProductUpdated',
        ];
    }

    public function __construct(MailerService $mailer)
    {
        $this->mailer = $mailer;
    }

    public function onProductUpdated(ChangedProductEvent $event): void
    {
        file_put_contents('/home/ivan.puzatkin/projects/smarketplace/cat.log', var_export($event, true));
        $this->mailer->sendUpdatedProductInfo($event->getEvent(), $event->getUpdatedProduct());
    }
}