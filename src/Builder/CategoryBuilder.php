<?php

namespace App\Builder;

use App\DataFixtures\CategoryFixture;
use App\Entity\Category;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CategoryBuilder
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function build(CategoryFixture $fixture): Category
    {
        $category = new Category();
        $category->setTitle($fixture->title);
        $category->setExternalId($fixture->externalId);

        $errors = $this->validator->validate($category);
        if ($errors->count() !== 0)
        {
            throw new ValidationFailedException('', $errors);
        }

        return $category;
    }
}