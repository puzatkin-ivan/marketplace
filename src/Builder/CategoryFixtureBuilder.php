<?php

namespace App\Builder;

use App\DataFixtures\CategoryFixture;

class CategoryFixtureBuilder
{
    public function build(array $options): CategoryFixture
    {
        $fixture = new CategoryFixture();
        $fixture->title = $options['title'] ?? null;
        $fixture->externalId = $options['eid'] ?? null;

        return $fixture;
    }
}