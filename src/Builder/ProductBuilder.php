<?php

namespace App\Builder;

use App\DataFixtures\ProductFixture;
use App\Entity\Product;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ProductBuilder
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function build(ProductFixture $fixture): Product
    {
        $product = new Product();
        $product->setTitle($fixture->title);
        $product->setPrice($fixture->price);
        $product->setCategories($fixture->categories);
        $product->setExternalId($fixture->externalId);

        $errors = $this->validator->validate($product);
        if ($errors->count() !== 0)
        {
            throw new ValidationFailedException('', $errors);
        }

        return $product;
    }
}