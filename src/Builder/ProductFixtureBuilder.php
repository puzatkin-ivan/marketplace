<?php

namespace App\Builder;

use App\DataFixtures\ProductFixture;

class ProductFixtureBuilder
{
    public function build(array $options): ProductFixture
    {
        $fixture = new ProductFixture();
        $fixture->title = $options['title'] ?? null;
        $fixture->price = $options['price'] ?? null;
        $fixture->categories = $options['categories'] ?? [];
        $fixture->externalId = $options['eid'] ?? null;

        return $fixture;
    }
}