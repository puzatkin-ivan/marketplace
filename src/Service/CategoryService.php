<?php

namespace App\Service;

use App\Builder\CategoryBuilder;
use App\DataFixtures\CategoryFixture;
use App\Entity\Category;
use App\Repository\CategoryRepository;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CategoryService
{
    /**
     * @var CategoryRepository
     */
    private $repo;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(CategoryRepository $repository, ValidatorInterface $validator)
    {
        $this->validator = $validator;
        $this->repo = $repository;
    }

    public function create(CategoryFixture $fixture): Category
    {
        $builder = new CategoryBuilder($this->validator);

        $category = $builder->build($fixture);

        $this->repo->add($category);

        return $category;
    }

    public function update(int $categoryId, CategoryFixture $fixture): Category
    {
        $category = $this->repo->get($categoryId);

        $category->setTitle($fixture->title);
        $category->setExternalId($fixture->externalId);

        $this->repo->update($category);
        return $category;
    }

    public function get(int $categoryId): ?Category
    {
        return $this->repo->get($categoryId);
    }

    public function delete(int $categoryId): void
    {
        $this->repo->delete($categoryId);
    }
}