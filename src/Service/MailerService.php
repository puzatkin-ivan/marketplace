<?php

namespace App\Service;

use App\Entity\Product;
use Swift_Mailer;
use Swift_Message;
use Twig\Environment;

class MailerService
{
    /**
     * @var Swift_Mailer
     */
    private $mailer;
    /**
     * @var Environment
     */
    private $twig;

    public function __construct(Swift_Mailer $mailer, Environment $environment)
    {
        $this->mailer = $mailer;
        $this->twig = $environment;
    }

    public function sendUpdatedProductInfo(string $event, Product $product): void
    {
        $body = $this->twig->render('mail/product_info.html.twig', [
            'event' => ucfirst($event),
            'product' => $product
        ]);

        $message = new Swift_Message();
        $message
            ->setSubject(ucfirst($event) . ' product.')
            ->setFrom(getenv('FROM_ADDRESS'))
            ->setTo(getenv('EMAIL_ADDRESS_FOR_UPDATED_PRODUCT'))
            ->setBody($body);
        file_put_contents('result.log', var_export($message, true));

        $this->mailer->send($message);
    }
}