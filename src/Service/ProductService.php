<?php

namespace App\Service;

use App\Builder\ProductBuilder;
use App\DataFixtures\ProductFixture;
use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ProductService
{
    /**
     * @var ProductRepository
     */
    private $repo;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(ProductRepository $repository, ValidatorInterface $validator)
    {
        $this->validator = $validator;
        $this->repo = $repository;
    }

    public function create(ProductFixture $fixture): Product
    {
        $builder = new ProductBuilder($this->validator);

        $product = $builder->build($fixture);

        $this->repo->add($product);

        return $product;
    }

    public function update(int $productId, ProductFixture $fixture): Product
    {
        $product = $this->repo->get($productId);

        $product->setTitle($fixture->title);
        $product->setPrice($fixture->price);
        $product->setCategories($fixture->categories);
        $product->setExternalId($fixture->externalId);

        $this->repo->update($product);
        return $product;
    }

    public function get(int $productId): ?Product
    {
        return $this->repo->get($productId);
    }

    public function delete(int $productId): void
    {
        $this->repo->delete($productId);
    }
}