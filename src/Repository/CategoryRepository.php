<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(ManagerRegistry $registry, ValidatorInterface $validator)
    {
        $this->validator = $validator;
        parent::__construct($registry, Category::class);
    }

    public function add(Category $category): void
    {
        $errors = $this->validator->validate($category);
        if ($errors->count() !== 0)
        {
            throw new ValidationFailedException('Category isn\'t valid. ', $errors);
        }

        $entityManager = $this->getEntityManager();
        $entityManager->persist($category);

        $entityManager->flush();
    }

    public function get(int $categoryId): ?Category
    {
        return $this->find($categoryId);
    }

    /**
     * @param int $categoryId
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(int $categoryId): void
    {
        $category = $this->find($categoryId);

        if (!$category)
        {
            throw new \OutOfRangeException("Category with {$categoryId} isn't exists.");
        }

        $this->getEntityManager()->remove($category);
        $this->getEntityManager()->flush();
    }

    public function update(Category $category): void
    {
        $errors = $this->validator->validate($category);
        if ($errors->count() !== 0)
        {
            throw new ValidationFailedException('Category isn\'t valid. ', $errors);
        }

        $this->getEntityManager()->flush();
    }
}
