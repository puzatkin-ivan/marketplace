<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(ManagerRegistry $registry, ValidatorInterface $validator)
    {
        $this->validator = $validator;
        parent::__construct($registry, Product::class);
    }

    public function add(Product $product): void
    {
        $errors = $this->validator->validate($product);
        if ($errors->count() !== 0)
        {
            throw new ValidationFailedException('Product isn\'t valid. ', $errors);
        }

        $entityManager = $this->getEntityManager();
        $entityManager->persist($product);

        $entityManager->flush();
    }

    public function get(int $productId): ?Product
    {
        return $this->find($productId);
    }

    /**
     * @param int $productId
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(int $productId): void
    {
        $product = $this->find($productId);

        if (!$product)
        {
            throw new \OutOfRangeException("Product with {$productId} isn't exists.");
        }

        $this->getEntityManager()->remove($product);
        $this->getEntityManager()->flush();
    }

    public function update(Product $product): void
    {
        $errors = $this->validator->validate($product);
        if ($errors->count() !== 0)
        {
            throw new ValidationFailedException('Product isn\'t valid. ', $errors);
        }

        $this->getEntityManager()->flush();
    }
}
