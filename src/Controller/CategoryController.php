<?php

namespace App\Controller;

use App\Builder\CategoryFixtureBuilder;
use App\DataFixtures\CategoryFixture;
use App\Service\CategoryService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CategoryController
{
    private const SUCCESS = 0;
    private const FAILED = 1;

    /**
     * @var CategoryService
     */
    private $service;

    public function __construct(CategoryService $service)
    {
        $this->service = $service;
    }

    public function create(Request $request): Response
    {
        $categoryJson = $request->get('category');
        $categoryFixture = $this->convertJsonToFixture(json_decode($categoryJson, true));

        try
        {
            $category = $this->service->create($categoryFixture);

            $response = [
                'code' => self::SUCCESS,
                'category' => $category ? $category->toArray() : null
            ];
        }
        catch (\Exception $ex)
        {
            $response = [
                'code' => self::FAILED,
                'errors' => $ex->getMessage(),
            ];
        }

        return new JsonResponse($response);
    }

    public function getCategory(Request $request): Response
    {
        $categoryId = $request->get('category_id');

        $category = $this->service->get($categoryId);

        $categoryArr = $category ? $category->toArray() : null;
        return new JsonResponse(['category' => $categoryArr]);
    }

    public function update(Request $request): Response
    {
        $categoryId = $request->get('category_id');

        $categoryJson = $request->get('category');
        $categoryFixture = $this->convertJsonToFixture(json_decode($categoryJson, true));

        try
        {
            $category = $this->service->update($categoryId, $categoryFixture);

            $response = [
                'code' => self::SUCCESS,
                'category' => $category ? $category->toArray() : null
            ];
        }
        catch (\Exception $ex)
        {
            $response = [
                'code' => self::FAILED,
                'errors' => $ex->getMessage(),
            ];
        }

        return new JsonResponse($response);
    }

    public function delete(Request $request): Response
    {
        $categoryId = $request->get('category_id');

        try
        {
            $this->service->delete($categoryId);
            $response = [
                'code' => self::SUCCESS
            ];
        }
        catch (\Exception $exception)
        {
            $response = [
                'code' => self::FAILED,
                'errors' => $exception->getMessage(),
            ];
        }

        return new JsonResponse($response);
    }

    private function convertJsonToFixture(array $options): CategoryFixture
    {
        $builder = new CategoryFixtureBuilder();

        return $builder->build($options);
    }
}