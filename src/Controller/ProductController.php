<?php

namespace App\Controller;

use App\Builder\ProductFixtureBuilder;
use App\DataFixtures\ProductFixture;
use App\Event\ChangedProductEvent;
use App\Service\ProductService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends AbstractController
{
    private const SUCCESS = 0;
    private const FAILED = 1;

    /**
     * @var ProductService
     */
    private $service;

    public function __construct(ProductService $service)
    {
        $this->service = $service;
    }

    public function create(Request $request, EventDispatcherInterface $eventDispatcher): Response
    {
        $productJson = $request->get('product');
        $productFixture = $this->convertJsonToFixture(json_decode($productJson, true));

        try
        {
            $product = $this->service->create($productFixture);

            $event = new ChangedProductEvent('create', $product);
            $eventDispatcher->dispatch($event);

            $response = [
                'code' => self::SUCCESS,
                'product' => $product ? $product->toArray() : null
            ];
        }
        catch (\Exception $ex)
        {
            $response = [
                'code' => self::FAILED,
                'errors' => $ex->getMessage(),
            ];
        }

        return new JsonResponse($response);
    }

    public function getProduct(Request $request): Response
    {
        $productId = $request->get('product_id');

        $product = $this->service->get($productId);

        $productArr = $product ? $product->toArray() : null;
        return new JsonResponse(['product' => $productArr]);
    }

    public function update(Request $request, EventDispatcherInterface $eventDispatcher): Response
    {
        $productId = $request->get('product_id');

        $productJson = $request->get('product');
        $productFixture = $this->convertJsonToFixture(json_decode($productJson, true));

        try
        {
            $product = $this->service->update($productId, $productFixture);

            $event = new ChangedProductEvent('update', $product);
            $eventDispatcher->dispatch($event);

            $response = [
                'code' => self::SUCCESS,
                'product' => $product ? $product->toArray() : null
            ];
        }
        catch (\Exception $ex)
        {
            $response = [
                'code' => self::FAILED,
                'errors' => $ex->getMessage(),
            ];
        }

        return new JsonResponse($response);
    }

    public function delete(Request $request): Response
    {
        $productId = $request->get('product_id');

        try
        {
            $this->service->delete($productId);
            $response = [
                'code' => self::SUCCESS
            ];
        }
        catch (\Exception $exception)
        {
            $response = [
                'code' => self::FAILED,
                'errors' => $exception->getMessage(),
            ];
        }

        return new JsonResponse($response);
    }

    private function convertJsonToFixture(array $options): ProductFixture
    {
        $builder = new ProductFixtureBuilder();

        return $builder->build($options);
    }
}