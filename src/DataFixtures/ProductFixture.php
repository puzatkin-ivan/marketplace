<?php

namespace App\DataFixtures;

class ProductFixture
{
    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $price;

    /**
     * @var array
     */
    public $categories;

    /**
     * @var int
     */
    public $externalId;
}