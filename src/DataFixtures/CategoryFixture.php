<?php

namespace App\DataFixtures;

class CategoryFixture
{
    /**
     * @var string
     */
    public $title;

    /**
     * @var int
     */
    public $externalId;
}