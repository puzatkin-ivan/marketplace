<?php

namespace App\Command;

use App\Builder\CategoryBuilder;
use App\Builder\CategoryFixtureBuilder;
use App\Builder\ProductBuilder;
use App\Builder\ProductFixtureBuilder;
use App\Converter\JsonToFixturesConverter;
use App\Entity\Category;
use App\Entity\Product;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use App\Service\CategoryService;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UploadProductsAndCategoriesFromJsonCommand extends Command
{
    protected static $defaultName = 'app:upload';

    /**
     * @var ManagerRegistry
     */
    private $doctrine;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(ManagerRegistry $doctrine, ValidatorInterface $validator, string $name = null)
    {
        $this->doctrine = $doctrine;
        $this->validator = $validator;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setDescription("Upload a new products and categories");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $converter = new JsonToFixturesConverter();
        /** @var string $categoriesJson */
        $categoriesJson = file_get_contents('categories.json');
        $categoryFixtures = $converter->convert($categoriesJson, function(array $item) {
            return (new CategoryFixtureBuilder())->build($item);
        });

        $builder = new CategoryBuilder($this->validator);
        $categories = [];
        $errors = [];
        foreach ($categoryFixtures as $fixture)
        {
            try
            {
                $categories[] = $builder->build($fixture);
            }
            catch (ValidationFailedException $exception)
            {
                $errors[] = $exception->getMessage();
            }
        }

        if (count($errors) !== 0)
        {
            $output->writeln(var_export($errors, true));
        }
        else
        {
            /** @var CategoryRepository $repo */
            $repo = $this->doctrine->getRepository(Category::class);
            foreach ($categories as $category)
            {
                $repo->add($category);
            }
        }

        $productsJson = file_get_contents('products.json');
        $productFixtures = $converter->convert($productsJson, function(array $item) {
            return (new ProductFixtureBuilder())->build($item);
        });
        $builder = new ProductBuilder($this->validator);
        $products = [];
        $errors = [];
        foreach ($productFixtures as $fixture)
        {
            try
            {
                $products[] = $builder->build($fixture);
            }
            catch (ValidationFailedException $exception)
            {
                $errors[] = $exception->getMessage();
            }
        }
        if (count($errors) !== 0)
        {
            $output->writeln(var_export($errors, true));
            return Command::FAILURE;
        }
        else
        {
            /** @var ProductRepository $repo */
            $repo = $this->doctrine->getRepository(Product::class);
            foreach ($products as $product)
            {
                $repo->add($product);
            }
        }



        return Command::SUCCESS;
    }
}