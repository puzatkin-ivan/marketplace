<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $product_id;

    /**
     * @ORM\Column(type="string", length=12)
     * @Assert\Length(
     *      min = 3,
     *      max = 12,
     *      minMessage = "Your product title must be at least {{ limit }} characters long",
     *      maxMessage = "Your product title cannot be longer than {{ limit }} characters",
     *      allowEmptyString = false
     * )
     */
    private $title;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     * @Assert\Range(
     *      min = 0,
     *      max = 200,
     *      notInRangeMessage = "Product price must be between {{ min }} and {{ max }}",
     * )
     */
    private $price;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $categories = [];

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $external_id;

    public function getProductId(): ?int
    {
        return $this->product_id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCategories(): ?array
    {
        return $this->categories;
    }

    public function setCategories(array $categories): self
    {
        $this->categories = $categories;

        return $this;
    }

    public function getExternalId(): ?int
    {
        return $this->external_id;
    }

    public function setExternalId(?int $external_id): self
    {
        $this->external_id = $external_id;

        return $this;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->product_id,
            'title' => $this->title,
            'price' => $this->price,
            'categories' => $this->categories,
            'eid' => $this->external_id
        ];
    }
}
