<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CategoryRepository::class)
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $category_id;

    /**
     * @ORM\Column(type="string", length=12)
     * @Assert\Length(
     *      min = 3,
     *      max = 12,
     *      minMessage = "Category title must be at least {{ limit }} characters long",
     *      maxMessage = "Category product title cannot be longer than {{ limit }} characters",
     *      allowEmptyString = false
     * )
     */
    private $title;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $external_id;

    public function getCategoryId(): ?int
    {
        return $this->category_id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getExternalId(): ?int
    {
        return $this->external_id;
    }

    public function setExternalId(?int $external_id): self
    {
        $this->external_id = $external_id;

        return $this;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->category_id,
            'title' => $this->title,
            'eid' => $this->external_id
        ];
    }
}
