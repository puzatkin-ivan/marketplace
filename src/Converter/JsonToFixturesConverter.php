<?php

namespace App\Converter;

use App\DataFixtures\CategoryFixture;
use App\DataFixtures\ProductFixture;

class JsonToFixturesConverter
{
    /**
     * @param string $json
     * @param callable $buildCallback
     * @return CategoryFixture[]|ProductFixture[]
     */
    public function convert(string $json, callable $buildCallback): array
    {
        $data = json_decode($json, true);

        $result = [];
        /** @var array $item */
        foreach ($data as $item)
        {
            $result[] = call_user_func($buildCallback, $item);
        }

        return $result;
    }
}