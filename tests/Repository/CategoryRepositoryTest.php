<?php

namespace App\Tests\Repository;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Exception\ValidationFailedException;

class CategoryRepositoryTest extends KernelTestCase
{
    /**
     * @var CategoryRepository
     */
    private $repo;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        /** @var EntityManager $em */
        $em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->repo = $em->getRepository(Category::class);
    }

    protected function tearDown(): void
    {
        $this->repo = null;
        parent::tearDown();
    }

    public function testAddCheckMinLengthTitle(): void
    {
        $this->expectException(ValidationFailedException::class);

        $product = new Category();
        $product->setTitle('Al');

        $this->repo->add($product);
    }

    public function testAddCheckMaxLengthTitle(): void
    {
        $this->expectException(ValidationFailedException::class);

        $product = new Category();
        $product->setTitle('The title more 12 characters');

        $this->repo->add($product);
    }

    public function testAdd(): void
    {
        $product = new Category();
        $product->setTitle('Desktop');

        $this->repo->add($product);

        $this->assertNotEmpty($product->getCategoryId());
    }

    public function testGet(): void
    {
        $expectedCategory = new Category();
        $expectedCategory->setTitle('Title');

        $this->repo->add($expectedCategory);
        $actualCategory = $this->repo->get($expectedCategory->getCategoryId());

        $this->assertEquals($expectedCategory, $actualCategory);
    }

    public function testDeleteNonexistentProduct(): void
    {
        $this->expectException(\OutOfRangeException::class);

        $this->repo->delete(0);
    }

    public function testDeleteExistingProduct(): void
    {
        $product = new Category();
        $product->setTitle('Title');

        $this->repo->add($product);

        $productId = $product->getCategoryId();
        $this->repo->delete($product->getCategoryId());

        $this->assertEmpty($this->repo->get($productId));
    }

    public function testUpdateProductToInvalidProduct(): void
    {
        $category = new Category();
        $category->setTitle('Valid');

        $this->repo->add($category);

        $category->setTitle('In');

        $this->expectException(ValidationFailedException::class);
        $this->repo->update($category);
    }

    public function testUpdate(): void
    {
        $category = new Category();
        $category->setTitle('Valid');

        $this->repo->add($category);

        $category->setTitle('Validity');
        $this->repo->update($category);

        $this->assertEquals('Validity', $category->getTitle());
    }
}
