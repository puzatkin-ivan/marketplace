<?php

namespace App\Tests\Repository;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Exception\ValidationFailedException;

class ProductRepositoryTest extends KernelTestCase
{
    /**
     * @var ProductRepository
     */
    private $repo;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        /** @var EntityManager $em */
        $em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->repo = $em->getRepository(Product::class);
    }

    protected function tearDown(): void
    {
        $this->repo = null;
        parent::tearDown();
    }

    public function testAddCheckMinLengthTitle(): void
    {
        $this->expectException(ValidationFailedException::class);

        $product = new Product();
        $product->setTitle('Ab');
        $product->setPrice('100.00');

        $this->repo->add($product);
    }

    public function testAddCheckMaxLengthTitle(): void
    {
        $this->expectException(ValidationFailedException::class);

        $product = new Product();
        $product->setTitle('The title more 12 characters');
        $product->setPrice('100.00');

        $this->repo->add($product);
    }

    public function testAddCheckMinValuePrice(): void
    {
        $this->expectException(ValidationFailedException::class);

        $product = new Product();
        $product->setTitle('Title');
        $product->setPrice('-0.1');

        $this->repo->add($product);
    }

    public function testAddCheckMaxValuePrice(): void
    {
        $this->expectException(ValidationFailedException::class);

        $product = new Product();
        $product->setTitle('Abc');
        $product->setPrice('200.01');

        $this->repo->add($product);
    }

    public function testAddProductIsValid(): void
    {
        $product = new Product();
        $product->setTitle('Title');
        $product->setPrice('10');

        $this->repo->add($product);

        $this->assertNotEmpty($product->getProductId());
    }

    public function testGet(): void
    {
        $expectedProduct = new Product();
        $expectedProduct->setTitle('Title');
        $expectedProduct->setPrice('10');

        $this->repo->add($expectedProduct);
        $actualProduct = $this->repo->get($expectedProduct->getProductId());

        $this->assertEquals($expectedProduct, $actualProduct);
    }

    public function testUpdateProductToInvalidProduct(): void
    {
        $product = new Product();
        $product->setTitle('Valid');
        $product->setPrice('100');

        $this->repo->add($product);

        $product->setTitle('In');
        $product->setPrice('1000');

        $this->expectException(ValidationFailedException::class);
        $this->repo->update($product);
    }

    public function testUpdate(): void
    {
        $product = new Product();
        $product->setTitle('Valid');
        $product->setPrice('100');

        $this->repo->add($product);

        $product->setTitle('Validity');
        $product->setPrice('100');
        $this->repo->update($product);

        $this->assertEquals('Validity', $product->getTitle());
    }

    public function testDeleteNonexistentProduct(): void
    {
        $this->expectException(\OutOfRangeException::class);

        $this->repo->delete(0);
    }

    public function testDeleteExistingProduct(): void
    {
        $product = new Product();
        $product->setTitle('Title');
        $product->setPrice('100');

        $this->repo->add($product);

        $productId = $product->getProductId();
        $this->repo->delete($product->getProductId());

        $this->assertEmpty($this->repo->get($productId));
    }
}
